#include <stdio.h>
#include <stdlib.h>
#ifdef _WIN32
	#include <windows.h>
#else
	#include <unistd.h>							// for usleep version of waait()
	//#include <time.h>							// for nanosleep version of waait()
#endif

#define MANIFEST_FILE		    "manifest.txt"
#define MANIFEST_SECTIONS	    8
#define USER_INPUT_CHAR_LIMIT   12				// real limit is one less
#define USER_INPUT_WORD_LIMIT	2
#define SCREEN_WIDTH		    71
#define	SCREEN_HEIGHT		    25
#define PLAYER					"\033[48;5;160m\033[38;5;255mX"	// how player looks
#define PLAYER_POS_X			19				// initial position on map (start with 0)
#define PLAYER_POS_Y			13
#define PLAYER_CENTER_X			36				// 36center position on screen
#define PLAYER_CENTER_Y			13				// 13ansi counting start from 1
#define MAX_STEPS_X				30
#define MAX_STEPS_Y				10	
#define PANEL_HEIGHT			11				// info / question panel			


#define FG 				0
#define BG 				1

#define SEC_FG          0
#define SEC_BG          1
#define SEC_COL         2
#define SEC_CON			3
#define SEC_ROOM		4
#define SEC_PROB		5
#define SEC_DIAL		6
#define SEC_OBS			7

#define YES				1
#define NO				0

typedef struct VEC2 {
	int	x,y;
} VEC2; 

typedef struct MANIFEST {
	int	sections;
	int rows;
	char ***matrix;
} MANIFEST; 

typedef struct CONTENT_FILE_DATA {
	int	width;
	int height;
	char **matrix;
} CONTENT_FILE_DATA;

typedef struct DIALOGUE {
	char **response1;
    char **response2;
    int *value;
	int *bool_state;
} DIALOGUE;

typedef struct OBSTRUCTION {
	char **warning;
	int *bool_state;
} OBSTRUCTION;

typedef struct MAPDATA {
    int ****color;      //access fg and bg with 0-1 consts
    int ***collision;
	char ***content;
	int *width;
	int *height;
	int ***next_room;
	char ****problem_solved;
	int **problem_solution;
	int **problem_state;
	char ****response;
	int **response_requirement;
	int **response_state;
	char ***obstruction_warning;
	int **obstruction_state;
} MAPDATA;

typedef struct INPUTDATA {
	int size;
	char **command;
} INPUTDATA;


#ifdef _WIN32

	void waait( int miliseconds ) {
		Sleep( miliseconds );
	}

#else
	// usleep works with C99, but -Werror doesn't like it since it is deprecated
	// compile with -std=gnu99 to get around this error
	// nanosleep is not deprecated, but c99 doesn't like it
	// choose which one you want. usleep needs unistd.h, and nanosleep needs time.h

	void waait( int miliseconds ) {
		usleep( 1000 * miliseconds );
	}
	/*
	void waait( int miliseconds ) {
		struct timespec tim;
		tim.tv_sec = 0;
   		tim.tv_nsec = (long)(miliseconds * 1000000);
		nanosleep(&tim, NULL);
	}
	*/
#endif


int stringlen(char *str) {						// strlen
	int len = 0;
	while(*str != '\0') {
		str++;
		len++;
	}
	return len;
}

int str_compare(char* string, char* constant) {
    int constant_length = stringlen(constant);
    int string_length = stringlen(string);
    if (constant_length != string_length) {
        return 0;
    }
    for (int i = 0; i < constant_length; i++) {
        if (constant[i] != string[i]) {
            return 0;
        }
    }
    return 1;
}

int str_is_int(char *str) {
	int num;
	int length = stringlen(str);
	if (length) {
		for (int i = 0; i < length; i++) {
			num = str[i] - '0';
			if ((num < 0) || (num > 9)) {
				return 0;
			}
		}
		return 1;
	}
	else {
		return 0;
	}
}

int str_to_int(char *str) {
	int result = 0;
	int length = stringlen(str);
	for (int i = 0; i < length; i++) {
		result = (result * 10) + (str[i] - '0');
	}
	return result;
}

MANIFEST read_manifest_file(char *file_name) {
	/*	- turns manifest file into a 2d array of strings
		- each file set (FG, BG...) is put in own index */
	MANIFEST data;
	int sections = MANIFEST_SECTIONS;
	int rows;
	char *str;
	char ***matrix;
	FILE *txt_file;

	//OPEN FILE
	txt_file = fopen(file_name, "r");
	if (txt_file == NULL) { 					// if error while opening
		fprintf(stderr, "(;^_^) Oopsie! Unable to open file -> %s\n", file_name);
		exit(1);
	}

	printf("Loading %s\n", file_name);

	//SET INTERMEDIARY CONTAINER FOR LINES READ
	str = (char *)malloc(100 * sizeof(char));

	//SET POINTER FOR 3D ARRAY
	matrix = (char ***)malloc(sections * sizeof(char**));

	//SKIP FIRST LINE AND READ NUMBER OF LINES
	while(getc(txt_file) != '\n');             	// skip to end of line
	fscanf(txt_file,"%d", &rows);
	while(getc(txt_file) != '\n');				// skip to end of line or it gets included

	//FILL MATRIX WITH FILE DATA
	for(int s = 0; s < sections; s++) {
		matrix[s] = (char **)malloc(rows * sizeof(char*));
		for(int r = 0; r < rows; r++) {
			if (fgets(str, 100, txt_file) == NULL) {	//abort if line is missing
				printf("(;^_^) Oopsie! A line is missing from the manifest file!\n");
				exit(1);
			}
			int str_len = stringlen(str); 
			str[str_len - 1] = '\0';		// overwriting trailing newline with a null char while preserving size
			if (str[0] != '#') {
				matrix[s][r] = (char *)malloc(str_len * sizeof(char));
				for (int c = 0; c < str_len; c++) {
					matrix[s][r][c] = str[c];
				}
			}
			else {
				r--;						// if comment, do not advance counter
			}
		}	
	}

	//FREE INTERMEDIARY POINTER
	free(str);

	//CLOSE OPENED FILE
	fclose(txt_file);

	//FILL STRUCT WITH DATA
	data.sections = sections;
	data.rows = rows;
	data.matrix = matrix;

	//RETURN STRUCT (matrix should be freed later)
	return data;
}

int** img_to_matrix(char *file_name) {
    /*  - Takes a .pgm or .ppm file and returns a 2d array of the values.
	    - .ppm files are converted from RGB to an 8-bit ANSI code range from 16-232 */
		
	int rows, columns, type, max_val;
	char ch;
	int **matrix;
	FILE *img_file;
	
	//OPEN FILE
	img_file = fopen(file_name, "rb");			// r->rb (fseek fails in Win)
	if (img_file == NULL) { 					// if error while opening
		fprintf(stderr, "(;^_^) Oopsie! Unable to open file -> %s\n", file_name); 
		exit(1);
	}
	
	printf("Loading %s\n", file_name);

	//CHECK WHETHER FILE STARTS WITH "P"
	ch = getc(img_file);
	if(ch != 'P') {
		printf("(;^_^) Oopsie! PGM/PPM files need to start with a 'P'!\n");
		exit(1);
	}

	//CHECK WHETHER FILE IS PGM or PPM, ASCII or RAW
	ch = getc(img_file);
	type = ch - 48; 							// 48 is 0 in ascii, so subtracting it from a string-number, we get a real int
	if((type != 2) && (type != 3) && (type != 5) && (type != 6)) {
		printf("(;^_^) Oopsie! Only 'P2'-(PGM ASCII), 'P3'-(PPM ASCII), 'P5'-(PGM RAW), and 'P6'-(PPM RAW) magic numbers accepted!\n");
		exit(1);
	}

	//SKIP USELESS LINES
	while(getc(img_file) != '\n');             	// skip to end of line
 	while (getc(img_file) == '#') {            	// skip comment lines 
		while (getc(img_file) != '\n');        	// skip to end of comment line 		
	}
	fseek(img_file, -1, SEEK_CUR);				// back up 1 space. This is needed or columns would get the wrong value

	//READ USEFUL HEADER DATA
	fscanf(img_file,"%d", &columns);
	fscanf(img_file,"%d", &rows);
	fscanf(img_file,"%d", &max_val);

	//MAKE SURE VALUES ARE 8-BIT 
	if (max_val > 255) {						// if <256, PGM gray values are represented by 1 byte in the raw version
		printf("(;^_^) Oopsie! Only 8 bit values accepted. Maxval is not less than 256!\n");
		exit(1);
	}

	//SKIP TO NEXT \N
	while(getc(img_file) != '\n');				// skip to end of line. Might not be needed for ASCII

	//PGM ASCII
	if (type == 2) {
		matrix = (int **)malloc(rows * sizeof(int*));
		for (int r = 0; r < rows; r++) {
			matrix[r] = (int *)malloc(columns * sizeof(int));
			for (int c = 0; c < columns; c++) {
				fscanf(img_file,"%d", &matrix[r][c]);
			}
		}
	}

	//PGM RAW
	else if (type == 5) {
		matrix = (int **)malloc(rows * sizeof(int*));
		for (int r = 0; r < rows; r++) {
			matrix[r] = (int *)malloc(columns * sizeof(int));
			for (int c = 0; c < columns; c++) {
				matrix[r][c] = getc(img_file);
			}
		}
	}

	//PPM ASCII
	else if (type == 3) {
		matrix = (int **)malloc(rows * sizeof(int*));
		int red, green, blue;
		for (int r = 0; r < rows; r++) {
			matrix[r] = (int *)malloc(columns * sizeof(int));
			for (int c = 0; c < columns; c++) {
				//READ COLORS
				fscanf(img_file,"%d", &red);
				fscanf(img_file,"%d", &green);
				fscanf(img_file,"%d", &blue);
				//CONVERT RGB TO 8-BIT ANSI
				if (red 	< 55) 	{red 	= 55;}
				if (green 	< 55) 	{green 	= 55;}
				if (blue 	< 55) 	{blue 	= 55;}
				matrix[r][c] = 	16 + 	((red - 55) / 40) * 36 + 	// 0, 95, 135, 175, 215, 255
										((green - 55) / 40) * 6 + 
										((blue - 55) / 40);			// (0-15 and 232-255 are disregarded)
			}
		}
	}

	//PPM RAW
	else if (type == 6) {
		matrix = (int **)malloc(rows * sizeof(int*));
		int red, green, blue;
		for (int r = 0; r < rows; r++) {
			matrix[r] = (int *)malloc(columns * sizeof(int));
			for (int c = 0; c < columns; c++) {
				//READ COLORS
				red = getc(img_file);
				green = getc(img_file);
				blue = getc(img_file);
				//CONVERT RGB TO 8-BIT ANSI
				if (red 	< 55) 	{red 	= 55;}
				if (green 	< 55) 	{green 	= 55;}
				if (blue 	< 55) 	{blue 	= 55;}
				matrix[r][c] = 	16 + 	((red - 55) / 40) * 36 + 	// 0, 95, 135, 175, 215, 255
										((green - 55) / 40) * 6 + 
										((blue - 55) / 40); 		// (0-15 and 232-255 are disregarded)
			}
		}
	}

	//CLOSE OPENED FILE
	fclose(img_file);

	//RETURN POINTER TO MATRIX
	return(matrix);
}

int** read_room_transition_file(char *file_name) {
    /*each set of 3 nums (room id, spawn_x, spawn_y) is put in its own array*/
    int entries;
    const int NUMS_PER_SET = 3;
    int **matrix;
    FILE *room_file;

    //OPEN FILE
	room_file = fopen(file_name, "r");
	if (room_file == NULL) { 					// if error while opening
		fprintf(stderr, "(;^_^) Oopsie! Unable to open file -> %s\n", file_name);
		exit(1);
	}

	printf("Loading %s\n", file_name);

    //GET NR OF ENTRIES
    while(getc(room_file) != '\n');             	// skip first line
    fscanf(room_file,"%d", &entries);
    while(getc(room_file) != '\n');             	// skip to end of line
    while(getc(room_file) != '\n')	             	// skip third line
		;
    //SET POINTER FOR 2D ARRAY
	matrix = (int **)malloc(entries * sizeof(int*));

    //FILL 2D ARRAY
    for (int i = 0; i < entries; i++) {
	    matrix[i] = (int *)malloc(NUMS_PER_SET * sizeof(int));
        fscanf(room_file,"%d", &matrix[i][0]);
        fscanf(room_file,"%d", &matrix[i][1]);
        fscanf(room_file,"%d", &matrix[i][2]);
        while(getc(room_file) != '\n');             	// skip to end of line
    }

	//CLOSE OPENED FILE
	fclose(room_file);

    //RETURN 2D ARRAY
    return matrix;
}

DIALOGUE read_dialogue_file(char *file_name) {
    /*returns a struct with strings for the label and a value*/
    int entries, string_len;
    char ***matrix;
    int *values;
	int *bool_state;	//make an array of 0 values for flipping later when problem is solved or obstruction cleared
    char *str;
    const int RESPONSE_ONE = 0;
    const int RESPONSE_TWO = 1;
    DIALOGUE result;
    FILE *dialogue_file;
    
    //OPEN FILE
	dialogue_file = fopen(file_name, "r");
	if (dialogue_file == NULL) { 					// if error while opening
		fprintf(stderr, "(;^_^) Oopsie! Unable to open file -> %s\n", file_name);
		exit(1);
	}

	printf("Loading %s\n", file_name);

    //GET NR OF ENTRIES
    while(getc(dialogue_file) != '\n');             	// skip first line
    fscanf(dialogue_file,"%d", &entries);
    while(getc(dialogue_file) != '\n');             	// skip to end of line
    while(getc(dialogue_file) != '\n')	             	// skip third line
		;

    //SET POINTERS
	matrix = (char ***)malloc(2 * sizeof(char**));
    matrix[RESPONSE_ONE] = (char **)malloc(entries * sizeof(char*));
    matrix[RESPONSE_TWO] = (char **)malloc(entries * sizeof(char*));
    values = (int *)malloc(entries * sizeof(int));
	bool_state = (int *)malloc(entries * sizeof(int));

    //MAKE INTERMEDIARY VARIABLE
    str = (char *)malloc((SCREEN_WIDTH * (PANEL_HEIGHT - 1)) * sizeof(char));

    //FILL 2D ARRAY
    for (int i = 0; i < entries; i++) {
        //FIRST STRING
        fgets(str, (SCREEN_WIDTH * (PANEL_HEIGHT - 1)), dialogue_file);
        for (int c = 0; c < (SCREEN_WIDTH * (PANEL_HEIGHT - 1)); c++) {
            if (str[c] == '\n') {
                str[c] = '\0';
                string_len = c + 1;     //extra space for null
                break;
            }
        }
	    matrix[RESPONSE_ONE][i] = (char *)malloc(string_len * sizeof(char));
        for (int c = 0; c < string_len; c++) {
            matrix[RESPONSE_ONE][i][c] = str[c];
        }
        //SECOND STRING
        fgets(str, (SCREEN_WIDTH * (PANEL_HEIGHT - 1)), dialogue_file);
        for (int c = 0; c < (SCREEN_WIDTH * (PANEL_HEIGHT - 1)); c++) {
            if (str[c] == '\n') {
                str[c] = '\0';
                string_len = c + 1;     //extra space for null
                break;
            }
        }
	    matrix[RESPONSE_TWO][i] = (char *)malloc(string_len * sizeof(char));
        for (int c = 0; c < string_len; c++) {
            matrix[RESPONSE_TWO][i][c] = str[c];
        }
        //NUMBER
        fscanf(dialogue_file,"%d", &values[i]);
        while(getc(dialogue_file) != '\n')	             	// skip to end of line
			;
		//BOOL STATE
		bool_state[i] = 0;
    }

    //FREE INTERMEDIARY VARIABLE
    free(str);
	//CLOSE OPENED FILE
	fclose(dialogue_file);

    //FILL STRUCT
    result.response1 = matrix[RESPONSE_ONE];
    result.response2 = matrix[RESPONSE_TWO];
    result.value = values;
	result.bool_state = bool_state;

    //FREE TOP OF MATRIX
    free(matrix);

    //RETURN RESULT
    return result;
}

OBSTRUCTION read_obstruction_file(char *file_name) {
    /*returns string for lable when the player hits the obstruction, and 
    the requirement for being able to pass it*/
    int entries, string_len;
    char **matrix;
	int *bool_state;	//make an array of 0 values for flipping later when problem is solved or obstruction cleared
    char *str;
    OBSTRUCTION result;
    FILE *obstruction_file;
    
    //OPEN FILE
	obstruction_file = fopen(file_name, "r");
	if (obstruction_file == NULL) { 					// if error while opening
		fprintf(stderr, "(;^_^) Oopsie! Unable to open file -> %s\n", file_name);
		exit(1);
	}

	printf("Loading %s\n", file_name);

    //GET NR OF ENTRIES
    while(getc(obstruction_file) != '\n');             	// skip first line
    fscanf(obstruction_file,"%d", &entries);
    while(getc(obstruction_file) != '\n');             	// skip to end of line
    while(getc(obstruction_file) != '\n')             	// skip third line
		;

    //SET POINTERS
	matrix = (char **)malloc(entries * sizeof(char*));
	bool_state = (int *)malloc(entries * sizeof(int));

    //MAKE INTERMEDIARY VARIABLE
    str = (char *)malloc((SCREEN_WIDTH * (PANEL_HEIGHT - 1)) * sizeof(char));

    //FILL 2D ARRAY
    for (int i = 0; i < entries; i++) {
        //FIRST STRING
        fgets(str, (SCREEN_WIDTH * (PANEL_HEIGHT - 1)), obstruction_file);
        for (int c = 0; c < (SCREEN_WIDTH * (PANEL_HEIGHT - 1)); c++) {
            if (str[c] == '\n') {
                str[c] = '\0';
                string_len = c + 1;     //extra space for null
                break;
            }
        }
	    matrix[i] = (char *)malloc(string_len * sizeof(char));
        for (int c = 0; c < string_len; c++) {
            matrix[i][c] = str[c];
        }
		//BOOL_STATE
		bool_state[i] = 0;
    }

    //FREE INTERMEDIARY VARIABLE
    free(str);
	//CLOSE OPENED FILE
	fclose(obstruction_file);

    //FILL STRUCT
    result.warning = matrix;
	result.bool_state = bool_state;

    //RETURN RESULT
    return result;
}

CONTENT_FILE_DATA read_map_content_file(char *file_name) {
    /*  - reads map file line by line and puts each string of chars
        into an array so chars can be accessed with coordinates [x][y] */
    int columns, rows;
    char **matrix;
    char *str;
    int str_len;
    CONTENT_FILE_DATA data;
    FILE *content_file;

    //OPEN FILE
	content_file = fopen(file_name, "r");
	if (content_file == NULL) { 					// if error while opening
		fprintf(stderr, "(;^_^) Oopsie! Unable to open file -> %s\n", file_name);
		exit(1);
	}

	printf("Loading %s  |  ", file_name);

    //GET MAP DIMENSIONS
    while(getc(content_file) != '\n');             	// skip first line
    fscanf(content_file,"%d", &columns);
    fscanf(content_file,"%d", &rows);
    while(getc(content_file) != '\n');             	// skip to end of line
    while(getc(content_file) != '\n')             	// skip third line
		;

	//CHECK WHETHER MAP SIZE VALID
	if (columns < (SCREEN_WIDTH - 2) || rows < (SCREEN_HEIGHT - 2)) {
		printf("(;^_^) Oopsie! Minimum map dimensions should be %ix%i\n", (SCREEN_WIDTH - 2), (SCREEN_HEIGHT - 2));
        exit(1);
	}
	else {
		printf("Map dimensions: %ix%i\n", columns, rows);
	}

	//SET INTERMEDIARY CONTAINER FOR LINES READ
	str = (char *)malloc((columns + 2) * sizeof(char));     // +2 for \n and oversize check

	//SET POINTER FOR 3D ARRAY
	matrix = (char **)malloc(rows * sizeof(char*));

    //MAKE 2D MATRIX OF CHARS / 1D ARRAY OF STRINGS (access char with [x][y])
    for (int r = 0; r < rows; r++) {
        matrix[r] = (char *)malloc((columns + 1) * sizeof(char));

        //ABORT IF LINE IS MISSING 
        if (fgets(str, (columns + 3), content_file) == NULL) {  // +3 for \n, fgets(n-1), and oversize check
            printf("(;^_^) Oopsie! A line is missing from the content file!\n");
            exit(1);
        }

        //CHECK STRING VALIDITY AND ABORT IF THERE IS A PROBLEM
        str_len = stringlen(str);
        if (str_len == columns) {                   // if last char is \n 
            if (str[columns - 1] == '\n') {     
                printf("(;^_^) Oopsie! A char is missing on line %i !\n", (r + 1));
                exit(1);
            }
        }
        else if (str_len > columns) {        // if \n messes up the length
            if (str_len == (columns + 1)) {
                if (str[columns] == '\n') {
                    str[columns] = '\0';    // replace with a \0 to get a string (not needed actually)
                }
                else {
                    printf("(;^_^) Oopsie! Line %i has a char too many!\n", (r + 1));
                    exit(1);
                }
            }
            else {
                printf("(;^_^) Oopsie! Line %i has too many chars!\n", (r + 1));
                exit(1);
            }
        }
        else {
            printf("(;^_^) Oopsie! Line %i has too few chars!\n", (r + 1));
            exit(1);
        }

        //COPY CHARS INTO CURRENT MATRIX LINE
        for (int c = 0; c < columns; c++) {
            matrix[r][c] = str[c];      
            str[c] = '\0';              // reset so old chars don't interfere with next loop     
        }
        matrix[r][columns] = '\0';      // add null at the end of the string 
        str[columns] = '\0';            // reset so old chars don't interfere
        
    }

	//FREE INTERMEDIARY POINTER
	free(str);

	//CLOSE OPENED FILE
	fclose(content_file);    

    //FILL STRUCT WITH DATA
    data.width = columns;
	data.height = rows;
	data.matrix = matrix;

    //RETURN STRUCT WITH WIDTH, HEIGHT, AND THE 2D ARRAY OF CHARS
    return data;
}

MAPDATA generate_map_data() {
    MAPDATA map;
    int sections, rows;
	MANIFEST files;
	CONTENT_FILE_DATA content;
	DIALOGUE problem;		//char* problem, char* congratulations, int solution
	DIALOGUE dialogue;		//char* warning, char* congratulations, int requirement
	OBSTRUCTION obstruction;//char* warning, int requirement

	//GET MANIFEST DATA
	files = read_manifest_file(MANIFEST_FILE);

	//GET DIMENSIONS FROM MANIFEST
	sections = files.sections;
    rows = files.rows;				// nr of levels

    //SPLIT MAP COLOR INTO FG AND BG
    map.color = (int ****)malloc(2 * sizeof(int***)); //map.color[FG/BG]

	//SPLIT MATH PROBLEM_SOLVED INTO TRUE AND FALSE
	map.problem_solved = (char ****)malloc(2 * sizeof(char***));

	//SPLIT RESPONSE INTO TRUE AND FALSE
	map.response = (char ****)malloc(2 * sizeof(char***));

	//ALLOCATE MEMORY FOR MAP DATA TYPES
    map.color[FG] = (int ***)malloc(rows * sizeof(int**));
    map.color[BG] = (int ***)malloc(rows * sizeof(int**));
    map.collision = (int ***)malloc(rows * sizeof(int**));
	map.content = (char ***)malloc(rows * sizeof(char**));
	map.width = (int *)malloc(rows * sizeof(int));
	map.height = (int *)malloc(rows * sizeof(int));
	map.next_room = (int ***)malloc(rows * sizeof(int**));
	map.problem_solved[NO] = (char ***)malloc(rows * sizeof(char**));
	map.problem_solved[YES] = (char ***)malloc(rows * sizeof(char**));
	map.problem_solution = (int **)malloc(rows * sizeof(int*));
	map.problem_state = (int **)malloc(rows * sizeof(int*));
	map.response[NO] = (char ***)malloc(rows * sizeof(char**));
	map.response[YES] = (char ***)malloc(rows * sizeof(char**));
	map.response_requirement = (int **)malloc(rows * sizeof(int*));
	map.response_state = (int **)malloc(rows * sizeof(int*));
	map.obstruction_warning = (char ***)malloc(rows * sizeof(char**));
	map.obstruction_state = (int **)malloc(rows * sizeof(int*));

	//FILL MAP DATA
    for (int i = 0; i < rows; i++) {
        map.color[FG][i] = img_to_matrix(files.matrix[SEC_FG][i]);
        map.color[BG][i] = img_to_matrix(files.matrix[SEC_BG][i]);
        map.collision[i] = img_to_matrix(files.matrix[SEC_COL][i]);

		content = read_map_content_file(files.matrix[SEC_CON][i]);
		map.content[i] = content.matrix;
		map.width[i] = content.width;
		map.height[i] = content.height;

		map.next_room[i] = read_room_transition_file(files.matrix[SEC_ROOM][i]);

		problem = read_dialogue_file(files.matrix[SEC_PROB][i]);
		map.problem_solved[NO][i] = problem.response1;
		map.problem_solved[YES][i] = problem.response2;
		map.problem_solution[i] = problem.value;
		map.problem_state[i] = problem.bool_state;

		dialogue = read_dialogue_file(files.matrix[SEC_DIAL][i]);
		map.response[NO][i] = dialogue.response1;
		map.response[YES][i] = dialogue.response2;
		map.response_requirement[i] = dialogue.value;
		map.response_state[i] = dialogue.bool_state;

		obstruction = read_obstruction_file(files.matrix[SEC_OBS][i]);
		map.obstruction_warning[i] = obstruction.warning;
		map.obstruction_state[i] = obstruction.bool_state;

    }

	//FREE FILE MATRIX FROM MEMORY
	for (int s = (sections - 1); s >= 0; s--) {
		for (int r = (rows - 1); r >= 0; r--) {
			free(files.matrix[s][r]);
		}
		free(files.matrix[s]);
	}
	free(files.matrix);

	//RETURN THE MAP DATA
	return map;
}


void generate_border(){
    printf("\033[1;1H\033[48;5;235m\033[38;5;30m########################\033[38;5;36m\\\\===================//\033[48;5;235m\033[38;5;30m########################");
    for (int i = 2; i < SCREEN_HEIGHT; i++) {
        printf("\033[%i;1H\033[48;5;235m\033[38;5;30m#                                                                     #", i);
    }
    printf("\033[25;1H\033[48;5;235m\033[38;5;30m#######################################################################");
    fflush(stdout);
}

void reset_console() {
    printf("\033[2;26H\033[48;5;235m\033[38;5;36m\\\\ >>              \033[48;5;235m\033[38;5;36m//");
    printf("\033[3;27H\033[48;5;235m\033[38;5;36m\\\\===============//");
    printf("\033[2;32H\033[48;5;235m\033[38;5;36m");   //reset cursor to input place and reset colors
    fflush(stdout);
}

void console_blink() {
    for (int i = 0; i < 2; i++) {
        printf("\033[2;29H\033[48;5;235m\033[38;5;203m>> \033[48;5;203m          ");
        fflush(stdout);
        waait(100);
        printf("\033[2;29H\033[48;5;235m\033[38;5;36m>>           ");
        fflush(stdout);
        waait(100);
    }
}

void bad_input() {
	console_blink();
	reset_console();
}

void get_input(char *user_input) {
    fgets(user_input, USER_INPUT_CHAR_LIMIT, stdin);

    //remove trailing newline that fgets puts at the end
    for (int c = 0; c < USER_INPUT_CHAR_LIMIT; c++) {
        if (user_input[c] == '\n') {
            user_input[c] = '\0';
            break;
        }
    }
}

void make_input_data(INPUTDATA *input_data, char *user_input) {
	int string_index = 0;
	int word_offset = 0;

	for (int c = 0; c < USER_INPUT_CHAR_LIMIT; c++) {
		if (user_input[c] != '\0') {
			if (user_input[c] != ' ') {
				input_data->command[string_index][(c - word_offset)] = user_input[c];
			}
			else {
				input_data->command[string_index][(c - word_offset)] = '\0';
				if ((string_index + 1) < USER_INPUT_WORD_LIMIT) {
					string_index += 1;
					word_offset = c + 1;
				}
				else {
					input_data->size = USER_INPUT_WORD_LIMIT + 1;
					break;
				}
			}
		}
		else {
			input_data->command[string_index][c - (word_offset)] = '\0';
			input_data->size = string_index + 1;
			break;
		}
	}
}

void move_cursor_away() {	// stash it somewhere it won't bother anyone
	printf("\033[%i;%iH", SCREEN_HEIGHT, SCREEN_WIDTH);
}

void draw_map(MAPDATA *map, int map_id, VEC2 *screen_pos) {
	int start_y = 2;					// ansi cursor starts at 1, and 1 is border
	int end_y = SCREEN_HEIGHT - 1;		// -1 for border
	int start_x = 2;
	int end_x = SCREEN_WIDTH - 1;
	int offset_y = 0;
	int offset_x = 0;

	if (screen_pos->y < 0) {				// if need black lines on top 
		start_y += (screen_pos->y * (-1));	// move start pos of actual map
		for (int y = 2; y < start_y; y++) {
			for (int x = start_x; x <= end_x; x++) {
				printf("\033[%i;%iH\033[48;5;16m\033[38;5;16m ", y, x);
			}
		}
	}
	else {									// natural map offset
		offset_y = screen_pos->y;
	}

	if (screen_pos->x < 0) {				// if need black lines on left 
		start_x += (screen_pos->x * (-1));	// move start pos of actual map
		for (int y = start_y; y <= end_y; y++) {
			for (int x = 2; x < start_x; x++) {
				printf("\033[%i;%iH\033[48;5;16m\033[38;5;16m ", y, x);
			}
		}
	}
	else {
		offset_x = screen_pos->x;
	}

	if (screen_pos->x < map->width[map_id]) { // the position of the scr on the map can't be higher than map wdth
		if ((end_x - start_x + 1) > (map->width[map_id] - screen_pos->x)) {
			end_x = (2 - 1 + map->width[map_id] - screen_pos->x);
			for (int y = start_y; y <= end_y; y++) {
				for (int x = (end_x + 1); x < SCREEN_WIDTH; x++) {
					printf("\033[%i;%iH\033[48;5;16m\033[38;5;16m ", y, x);
				}
			}
		}
	}

	if (screen_pos->y < map->height[map_id]) {
		if ((end_y - start_y + 1) > (map->height[map_id] - screen_pos->y)) {
			end_y = (2 - 1 + map->height[map_id] - screen_pos->y);
			for (int y = (end_y + 1); y < SCREEN_HEIGHT; y++) {
				for (int x = start_x; x <= end_x; x++) {
					printf("\033[%i;%iH\033[48;5;16m\033[38;5;16m ", y, x);
				}
			}
		}
	}

	for (int y = 0; y <= (end_y - start_y); y++) {
		for (int x = 0; x <= end_x - start_x; x++) {		
			printf("\033[%i;%iH\033[48;5;%im\033[38;5;%im%c", 
				(y + start_y), (x + start_x),
				map->color[BG][map_id][(offset_y + y)][(offset_x + x)], 
				map->color[FG][map_id][(offset_y + y)][(offset_x + x)],
				map->content[map_id][(offset_y + y)][(offset_x + x)]
			);
		}
	}
	move_cursor_away();
	fflush(stdout);
}

void draw_player(int displacement_x, int displacement_y) {
	printf("\033[%i;%iH%s",(PLAYER_CENTER_Y + displacement_y), (PLAYER_CENTER_X + displacement_x), PLAYER);
	move_cursor_away();
	fflush(stdout);
}

VEC2 move_player(MAPDATA *map, int map_id, VEC2 *screen_pos, int horizontal, int vertical) {
	VEC2 screen_position;
	screen_position.x = screen_pos->x;
	screen_position.y = screen_pos->y;

	if (horizontal != 0) {
		int direction, steps;
		if (horizontal > 0) {
			direction = 1;
			steps = horizontal;
		}
		else {
			direction = -1;
			steps = horizontal * (-1);
		}
		// MOVE PLAYER
		for (int i = 1; i <= steps; i++) {
			waait(100);
			draw_map(map, map_id, screen_pos);
			draw_player((i * direction), 0);
			reset_console();
		}
		// MOVE CAMERA
		for (int i = 1; i <= steps; i++) {
			waait(150);
			screen_position.x += direction;
			draw_map(map, map_id, &screen_position);
			draw_player(((steps - i) * direction), 0);
			reset_console();
		}
	}
	else if (vertical !=0) {
		int direction, steps;
		if (vertical > 0) {
			direction = 1;
			steps = vertical;
		}
		else {
			direction = -1;
			steps = vertical * (-1);
		}
		// MOVE PLAYER
		for (int i = 1; i <= steps; i++) {
			waait(150);
			draw_map(map, map_id, screen_pos);
			draw_player(0, (i * direction));
			reset_console();
		}
		// MOVE CAMERA
		for (int i = 1; i <= steps; i++) {
			waait(200);
			screen_position.y += direction;
			draw_map(map, map_id, &screen_position);
			draw_player(0, ((steps - i) * direction));
			reset_console();
		}
	}

	return screen_position;	// changes screen_pos values in the main scope
}

void fill_panel_text(char *text, int margins) {
	int y_pos = (PANEL_HEIGHT - 1);
	int x_pos = 4 + margins;				// border, space, border
	int string_len = stringlen(text);
	
	for (int c = 0; c < string_len; c++) {
		if (text[c] != '\t') {
			printf("\033[%i;%iH%c", (SCREEN_HEIGHT - y_pos), x_pos, text[c]);
			x_pos++;
			if (x_pos > (SCREEN_WIDTH - (3 + margins))) {		//if exceeding max width 
				if (y_pos > 2) {	//bottom border and panel border
					y_pos--;
				}
				else {
					break;
				}
			}
		}
		else {
			if (y_pos > 2) {
				y_pos--;
				x_pos = 4 + margins;
			}
			else {
				break;
			}
		}
	}
}

void info_panel(MAPDATA *map, int map_id, VEC2 *screen_pos, char *text) {
	//POP UP
	for (int i = 0; i < PANEL_HEIGHT; i++) {
		waait(100);
		printf("\033[%i;3H\033[48;5;236m\033[38;5;43m###################################################################", (SCREEN_HEIGHT - 1 - i));
		for (int j = 0; j < i; j++) {
					printf("\033[%i;3H\033[48;5;236m\033[38;5;43m#                                                                 #", (SCREEN_HEIGHT - 1 - j));
		}
		printf("\033[%i;3H\033[48;5;236m\033[38;5;43m#####################################################\033[38;5;151m[ok]\033[38;5;43m##########", (SCREEN_HEIGHT - 1));
		fflush(stdout);
	}
	//PRINT TEXT
	printf("\033[38;5;151m");	//set the color
	fill_panel_text(text, 1);

	//LOOP LISTENING FOR CONFIRMATION/OK
	reset_console();
	int loop = 1;
	char input[USER_INPUT_CHAR_LIMIT];
	while (loop) {
		//GET INPUT	
		fgets(input, USER_INPUT_CHAR_LIMIT, stdin);
		for (int c = 0; c < USER_INPUT_CHAR_LIMIT; c++) {
			if (input[c] == '\n') {
				input[c] = '\0';
				break;
			}
		}
		//PROCESS INPUT
		if (str_compare(input, "") || str_compare(input, "ok")) {
			loop = 0;
			//POP DOWN
			for (int i = PANEL_HEIGHT - 1; i >= 0; i--) {
				draw_map(map, map_id, screen_pos);
				draw_player(0, 0);
				reset_console();
				printf("\033[%i;3H\033[48;5;236m\033[38;5;43m###################################################################", (SCREEN_HEIGHT - 1 - i));
				for (int j = 0; j < i; j++) {
							printf("\033[%i;3H\033[48;5;236m\033[38;5;43m#                                                                 #", (SCREEN_HEIGHT - 1 - j));
				}
				printf("\033[%i;3H\033[48;5;236m\033[38;5;43m#####################################################\033[38;5;151m[ok]\033[38;5;43m##########", (SCREEN_HEIGHT - 1));
				fflush(stdout);
				waait(100);
			}
		}
		else {
			bad_input();
		}
	}
	
}

void level_fadeout() {
	for (int i = 2; i <= ((SCREEN_WIDTH / 2) + 1); i++) {
		for (int j = 2; j < SCREEN_HEIGHT; j++) {
			printf("\033[%i;%iH\033[48;5;235m\033[38;5;235m ", j, i);
			printf("\033[%i;%iH\033[48;5;235m\033[38;5;235m ", j, (SCREEN_WIDTH - i + 1));
			move_cursor_away();
		}
		fflush(stdout);
		waait(20);
	}
}

void panel_blink_wrong_solution() {
	for (int loops = 0; loops < 3; loops++) {
		for (int i = 1; i < (PANEL_HEIGHT - 1); i++) {
			printf("\033[%i;4H\033[48;5;203m\033[38;5;16m                                                                 ", (SCREEN_HEIGHT - 1 - i));
		}
		fill_panel_text("		             ##   ##  ######  ######  ##  ##  ######	             ## # ##  ##  ##  ##  ##  ### ##  ##	             ## # ##  ####    ##  ##  ## ###  ## ###	             #######  ##  ##  ######  ##  ##  ######", 1);
		move_cursor_away();
		fflush(stdout);
		waait(200);
		for (int i = 1; i < (PANEL_HEIGHT - 1); i++) {
			printf("\033[%i;4H\033[48;5;16m\033[38;5;203m                                                                 ", (SCREEN_HEIGHT - 1 - i));
		}
		fill_panel_text("		             ##   ##  ######  ######  ##  ##  ######	             ## # ##  ##  ##  ##  ##  ### ##  ##	             ## # ##  ####    ##  ##  ## ###  ## ###	             #######  ##  ##  ######  ##  ##  ######", 1);
		move_cursor_away();
		fflush(stdout);
		waait(200);
	}
}

void problem_panel(MAPDATA *map, int map_id, VEC2 *screen_pos, int tile_value, int *score) {
	//POP UP
	for (int i = 0; i < PANEL_HEIGHT; i++) {
		waait(100);
		printf("\033[%i;3H\033[48;5;236m\033[38;5;43m###################################################################", (SCREEN_HEIGHT - 1 - i));
		for (int j = 0; j < i; j++) {
					printf("\033[%i;3H\033[48;5;236m\033[38;5;43m#                                                                 #", (SCREEN_HEIGHT - 1 - j));
		}
		printf("\033[%i;3H\033[48;5;236m\033[38;5;43m###################################################################", (SCREEN_HEIGHT - 1));
		fflush(stdout);
	}
	//PRINT TEXT
	printf("\033[38;5;151m");	//set the color
	fill_panel_text(map->problem_solved[NO][map_id][(tile_value - 100)], 1);

	//LOOP LISTENING FOR SOLUTION
	reset_console();
	int loop = 1;
	char input[USER_INPUT_CHAR_LIMIT];
	int expected_solution = map->problem_solution[map_id][(tile_value - 100)];

	//GET INPUT	
	fgets(input, USER_INPUT_CHAR_LIMIT, stdin);
	for (int c = 0; c < USER_INPUT_CHAR_LIMIT; c++) {
		if (input[c] == '\n') {
			input[c] = '\0';
			break;
		}
	}
	//PROCESS INPUT
	if (str_is_int(input)) {
		if (str_to_int(input) == expected_solution) {
			map->problem_state[map_id][(tile_value - 100)] = 1;
			*score += 1;			// IF SOLUTION CORRECT, INCREMENT SCORE
			printf("\033[%i;3H\033[48;5;236m\033[38;5;43m#####################################################\033[38;5;151m[ok]\033[38;5;43m##########", (SCREEN_HEIGHT - 1));
			for (int i = 1; i < (PANEL_HEIGHT - 1); i++) {
				printf("\033[%i;4H\033[48;5;36m\033[38;5;255m                                                                 ", (SCREEN_HEIGHT - 1 - i));
			}
			printf("\033[38;5;16m");	//set the color
			fill_panel_text(map->problem_solved[YES][map_id][(tile_value - 100)], 1);
			reset_console();
			while (loop) {
				fgets(input, USER_INPUT_CHAR_LIMIT, stdin);
				for (int c = 0; c < USER_INPUT_CHAR_LIMIT; c++) {
					if (input[c] == '\n') {
						input[c] = '\0';
						break;
					}
				}
				if (str_compare(input, "") || str_compare(input, "ok")) {
					loop = 0;
					//POP DOWN
					for (int i = PANEL_HEIGHT - 1; i >= 0; i--) {
						draw_map(map, map_id, screen_pos);
						draw_player(0, 0);
						reset_console();
						printf("\033[%i;3H\033[48;5;236m\033[38;5;43m###################################################################", (SCREEN_HEIGHT - 1 - i));
						for (int j = 0; j < i; j++) {
									printf("\033[%i;3H\033[48;5;236m\033[38;5;43m#                                                                 #", (SCREEN_HEIGHT - 1 - j));
						}
						printf("\033[%i;3H\033[48;5;236m\033[38;5;43m#####################################################\033[38;5;151m[ok]\033[38;5;43m##########", (SCREEN_HEIGHT - 1));
						fflush(stdout);
						waait(100);
					}
				}
				else {
					bad_input();
				}
			}
		}
		else {
			panel_blink_wrong_solution();
		}
	}
	else {
		panel_blink_wrong_solution();
	}
}

void finish() {
	//PAINT SCREEN BLACK
	for (int i = 2; i < SCREEN_HEIGHT; i++) {
		printf("\033[%i;2H\033[48;5;16m\033[38;5;16m                                                                     ", i);
	}
	//WRITE CONTENT
	printf("\033[9;2H\033[38;5;228m                            CONGRATULATIONS!");
	printf("\033[10;2H\033[38;5;228m                  YOU BECAME THE ROYAL MATHEMATICIAN");
	printf("\033[22;2H\033[38;5;228m                                  Press ENTER or [ok] to Quit!");

	fflush(stdout);

	reset_console();
	int loop = 1;
	char input[USER_INPUT_CHAR_LIMIT];
	while (loop) {
		//GET INPUT	
		fgets(input, USER_INPUT_CHAR_LIMIT, stdin);
		for (int c = 0; c < USER_INPUT_CHAR_LIMIT; c++) {
			if (input[c] == '\n') {
				input[c] = '\0';
				break;
			}
		}
		//PROCESS INPUT
		if (str_compare(input, "") || str_compare(input, "ok")) {
			loop = 0;
		}
		else {
			bad_input();
		}
	}
}

void handle_collision(MAPDATA *map, int *map_id, VEC2 *screen_pos, int tile_value, int *score, int *loop) {
	if (tile_value <= 49) {	// if 0 or obstruction tile
		//OBSTRUCTION (49-1, this one goes in reverse lest it be indistinguishable from 0)
		if (tile_value != 0) {	// check whether obstruction tile is free to pass, otherwise 0 means it is free
			if (map->obstruction_state[*map_id][(49 - tile_value)] == 0) {	//if we are not free to pass, we need to handle it, otherwise, handle like 0 - just move along
				info_panel(map, *map_id, screen_pos, map->obstruction_warning[*map_id][(49 - tile_value)]);
			}
		}
	}
	//ROOM CHANGE (50-99)
	else if (tile_value <= 99) {
		level_fadeout();
		screen_pos->x = map->next_room[*map_id][(tile_value - 50)][1] - (PLAYER_CENTER_X - 2);
		screen_pos->y = map->next_room[*map_id][(tile_value - 50)][2] - (PLAYER_CENTER_Y - 2);
		*map_id = map->next_room[*map_id][(tile_value - 50)][0];
		//LEVEL FADE-IN
		for (int i = ((SCREEN_HEIGHT / 2) + 1); i >= 2; i--) {
			draw_map(map, *map_id, screen_pos);
			for (int j = i; j >= 2; j--) {
				printf("\033[%i;2H\033[48;5;235m\033[38;5;235m                                                                     ", j);
				printf("\033[%i;2H\033[48;5;235m\033[38;5;235m                                                                     ", (SCREEN_HEIGHT - j + 1));
				move_cursor_away();
			}
			fflush(stdout);
			waait(70);
		}
	}
	//PROBLEM (100-199)
	else if (tile_value <= 199) {
		if (map->problem_state[*map_id][(tile_value - 100)] == 0) {	//problem needs solving - show problem
			problem_panel(map, *map_id, screen_pos, tile_value, score);
		}
		else {	//if problem solved - show congratulations
			info_panel(map, *map_id, screen_pos, map->problem_solved[YES][*map_id][(tile_value - 100)]);
		}
	}
	//DIALOGUE (200-254)	
	else if (tile_value <= 253) {
		if (map->response_state[*map_id][(tile_value - 200)] == 0) {	//IF RESPONSE IS NO, TEST IF REQUIREMENTS MET TO UNLOCK OBSTRUCTION
			if (map->response_requirement[*map_id][(tile_value - 200)] <= *score) {	//if the player has the score, but hasn't yet removed the obstruction
				map->response_state[*map_id][(tile_value - 200)] = 1;
				map->obstruction_state[*map_id][(tile_value - 200)] = 1;
				info_panel(map, *map_id, screen_pos, map->response[YES][*map_id][(tile_value - 200)]);
			}
			else {
				info_panel(map, *map_id, screen_pos, map->response[NO][*map_id][(tile_value - 200)]);
			}
		}
		else {
				info_panel(map, *map_id, screen_pos, map->response[YES][*map_id][(tile_value - 200)]);
		}
	}
	//FINISH GAME
	else if (tile_value == 254) {
		finish();
		*loop = 0;
	}
	//WALL (255)
	else {
			info_panel(map, *map_id, screen_pos, "\t\t\t\t                    (There is an obstacle!)");
	}
}

void quit() {
	//PAINT SCREEN BLACK
	for (int i = 2; i < SCREEN_HEIGHT; i++) {
		printf("\033[%i;2H\033[48;5;16m\033[38;5;16m                                                                     ", i);
	}
	//WRITE CONTENT
	printf("\033[7;2H\033[38;5;228m                          THANKS FOR PLAYING");
	fflush(stdout);
	waait(500);
	waait(500);
	waait(500);
	//RESET THE CURSOR SO IT DOESN?T STAY IN THE MIDDLE OF THE SCREEN
	printf("\033[26;1H\033[0m\n");
}

void help() {
	//PAINT SCREEN BLACK
	for (int i = 2; i < SCREEN_HEIGHT; i++) {
		printf("\033[%i;2H\033[48;5;16m\033[38;5;16m                                                                     ", i);
	}
	//WRITE CONTENT
	printf("\033[4;2H\033[38;5;228m   GENERAL:");
	printf("\033[6;2H\033[38;5;228m     [quit]  - quits the game");
	printf("\033[7;2H\033[38;5;228m     [pause] - pauses the game");
	printf("\033[9;2H\033[38;5;228m   MOVEMENT:");
	printf("\033[11;2H\033[38;5;228m     [l 7] or [left 7] - move left  a set amount of steps (max %i)", MAX_STEPS_X);
	printf("\033[12;2H\033[38;5;228m     [r 1] or [left 1] - move right a set amount of steps (max %i)", MAX_STEPS_X);
	printf("\033[13;2H\033[38;5;228m     [u 10] or [up 10] - move up    a set amount of steps (max %i)", MAX_STEPS_Y);
	printf("\033[14;2H\033[38;5;228m     [d 5] or [down 5] - move down  a set amount of steps (max %i)", MAX_STEPS_Y);
	printf("\033[16;2H\033[38;5;228m   (inputs are confirmed with ENTER)");
	printf("\033[22;2H\033[38;5;228m                        Press ENTER or [ok] to leave Help screen!");

	fflush(stdout);

	reset_console();
	int loop = 1;
	char input[USER_INPUT_CHAR_LIMIT];
	while (loop) {
		//GET INPUT	
		fgets(input, USER_INPUT_CHAR_LIMIT, stdin);
		for (int c = 0; c < USER_INPUT_CHAR_LIMIT; c++) {
			if (input[c] == '\n') {
				input[c] = '\0';
				break;
			}
		}
		//PROCESS INPUT
		if (str_compare(input, "") || str_compare(input, "ok")) {
			loop = 0;
		}
		else {
			bad_input();
		}
	}
}

void start() {
	//PAINT SCREEN BLACK
	for (int i = 2; i < SCREEN_HEIGHT; i++) {
		printf("\033[%i;2H\033[48;5;16m\033[38;5;16m                                                                     ", i);
	}
	//WRITE CONTENT
	printf("\033[4;2H\033[38;5;228m                         ### ### # # ### #  ");
	printf("\033[5;2H\033[38;5;228m                         # # # # # # # # #  ");
	printf("\033[6;2H\033[38;5;228m                         ### # #  #  ### #  ");
	printf("\033[7;2H\033[38;5;228m                         ##  # #  #  # # #  ");
	printf("\033[8;2H\033[38;5;228m                         # # ###  #  # # ###");

	printf("\033[10;2H\033[38;5;228m         # # ### ### # # ### # # ### ###  #  ###  #  ### # #");
	printf("\033[11;2H\033[38;5;228m         ### # #  #  # # #   ### # #  #   #  #    #  # # ###");
	printf("\033[12;2H\033[38;5;228m         ### ###  #  ### ##  ### ###  #   #  #    #  ### ###");
	printf("\033[13;2H\033[38;5;228m         # # # #  #  # # #   # # # #  #   #  #    #  # # ###");
	printf("\033[14;2H\033[38;5;228m         # # # #  #  # # ### # # # #  #   #  ###  #  # # # #");

	fflush(stdout);

	waait(500);
	waait(500);
	waait(500);
	waait(500);
	waait(500);
	waait(500);
	waait(500);
}


int main() {
	MAPDATA map = generate_map_data();
    int loop = 1;
    char *user_input;
    user_input = (char *)malloc(USER_INPUT_CHAR_LIMIT * sizeof(char));
	INPUTDATA input_data;
	input_data.command = (char **)malloc(USER_INPUT_WORD_LIMIT * sizeof(char*));
	input_data.command[0] = (char *)malloc(USER_INPUT_CHAR_LIMIT * sizeof(char));
	input_data.command[1] = (char *)malloc(USER_INPUT_CHAR_LIMIT * sizeof(char));
	int steps;
	int next_tile = 0;

	int current_map = 0;
	int score = 0;
	VEC2 screen_pos;
	screen_pos.x = PLAYER_POS_X - (PLAYER_CENTER_X - 2);
	screen_pos.y = PLAYER_POS_Y - (PLAYER_CENTER_Y - 2);

	generate_border();
	start();

    while (loop) {
		generate_border();
		draw_map(&map, current_map, &screen_pos);
		draw_player(0, 0);
		reset_console();
		next_tile = 0;

        get_input(user_input);
		make_input_data(&input_data, user_input);
		
		//PROCESS USER INPUT
		if (input_data.size == 1) {
			if (str_compare(input_data.command[0], "quit")) {
				quit();
				loop = 0;
			}
			else if (str_compare(input_data.command[0], "help")) {
				help();
			}
			else {
				bad_input();
			} 
		}
		else if (input_data.size == 2) {
			//RIGHT
			if (str_compare(input_data.command[0], "right") || str_compare(input_data.command[0], "r")) {
				if (str_is_int(input_data.command[1])) {
					steps = str_to_int(input_data.command[1]);
					if (steps <= MAX_STEPS_X) {			// we know it is positive, so no need for >0
						//CHECK WHETHER PATH FREE OF OBSTRUCTION
						for (int i = 1; i <= steps; i++) {	// test if path is free. if not, set next tile to the number so we can handle it
							next_tile = map.collision[current_map][(screen_pos.y + PLAYER_CENTER_Y - 2)][(screen_pos.x + PLAYER_CENTER_X - 2 + i)];
							if (next_tile > 0) {
								if (next_tile <= 49) {	//check whether obstruction is cleared
									if (map.obstruction_state[current_map][(49 - next_tile)] == 0) {	//if not cleared (0), stop movement
										steps = i - 1;
										break;
									}	
								}
								else {	//(50+ is always wall-like, so we stop walking)
									steps = i - 1;
									break;
								}
							}
						}
						//MOVE PLAYER ALLOWED NUMBER OF STEPS
						screen_pos = move_player(&map, current_map, &screen_pos, steps, 0);
						//IF THERE WAS OBSTRUCTION, HANDLE IT
						handle_collision(&map, &current_map, &screen_pos, next_tile, &score, &loop);
					}
					else {
						bad_input();
					}
				}
				else {
					bad_input();
				}
			}
			//LEFT
			else if (str_compare(input_data.command[0], "left") || str_compare(input_data.command[0], "l")) {
				if (str_is_int(input_data.command[1])) {
					steps = str_to_int(input_data.command[1]);
					if (steps <= MAX_STEPS_X) {			// we know it is positive, so no need for >0
						//CHECK WHETHER PATH FREE OF OBSTRUCTION
						for (int i = 1; i <= steps; i++) {	// test if path is free. if not, set next tile to the number so we can handle it
							next_tile = map.collision[current_map][(screen_pos.y + PLAYER_CENTER_Y - 2)][(screen_pos.x + PLAYER_CENTER_X - 2 - i)];
							if (next_tile > 0) {
								if (next_tile <= 49) {	//check whether obstruction is cleared
									if (map.obstruction_state[current_map][(49 - next_tile)] == 0) {	//if not cleared (0), stop movement
										steps = i - 1;
										break;
									}	
								}
								else {	//(50+ is always wall-like, so we stop walking)
									steps = i - 1;
									break;
								}
							}
						}
						//MOVE PLAYER ALLOWED NUMBER OF STEPS
						screen_pos = move_player(&map, current_map, &screen_pos, steps * (-1), 0);
						//IF THERE WAS OBSTRUCTION, HANDLE IT
						handle_collision(&map, &current_map, &screen_pos, next_tile, &score, &loop);
					}
					else {
						bad_input();
					}
				}
				else {
					bad_input();
				}
			}
			//DOWN
			else if (str_compare(input_data.command[0], "down") || str_compare(input_data.command[0], "d")) {
				if (str_is_int(input_data.command[1])) {
					steps = str_to_int(input_data.command[1]);
					if (steps <= MAX_STEPS_Y) {			// we know it is positive, so no need for >0
						//CHECK WHETHER PATH FREE OF OBSTRUCTION
						for (int i = 1; i <= steps; i++) {	// test if path is free. if not, set next tile to the number so we can handle it
							next_tile = map.collision[current_map][(screen_pos.y + PLAYER_CENTER_Y - 2 + i)][(screen_pos.x + PLAYER_CENTER_X - 2)];
							if (next_tile > 0) {
								if (next_tile <= 49) {	//check whether obstruction is cleared
									if (map.obstruction_state[current_map][(49 - next_tile)] == 0) {	//if not cleared (0), stop movement
										steps = i - 1;
										break;
									}	
								}
								else {	//(50+ is always wall-like, so we stop walking)
									steps = i - 1;
									break;
								}
							}
						}
						//MOVE PLAYER ALLOWED NUMBER OF STEPS
						screen_pos = move_player(&map, current_map, &screen_pos, 0, steps);
						//IF THERE WAS OBSTRUCTION, HANDLE IT
						handle_collision(&map, &current_map, &screen_pos, next_tile, &score, &loop);
					}
					else {
						bad_input();
					}
				}
				else {
					bad_input();
				}
			}
			//UP
			else if (str_compare(input_data.command[0], "up") || str_compare(input_data.command[0], "u")) {
				if (str_is_int(input_data.command[1])) {
					steps = str_to_int(input_data.command[1]);
					if (steps <= MAX_STEPS_Y) {			// we know it is positive, so no need for >0
						//CHECK WHETHER PATH FREE OF OBSTRUCTION
						for (int i = 1; i <= steps; i++) {	// test if path is free. if not, set next tile to the number so we can handle it
							next_tile = map.collision[current_map][(screen_pos.y + PLAYER_CENTER_Y - 2 - i)][(screen_pos.x + PLAYER_CENTER_X - 2)];
							if (next_tile > 0) {
								if (next_tile <= 49) {	//check whether obstruction is cleared
									if (map.obstruction_state[current_map][(49 - next_tile)] == 0) {	//if not cleared (0), stop movement
										steps = i - 1;
										break;
									}	
								}
								else {	//(50+ is always wall-like, so we stop walking)
									steps = i - 1;
									break;
								}
							}
						}
						//MOVE PLAYER ALLOWED NUMBER OF STEPS
						screen_pos = move_player(&map, current_map, &screen_pos, 0, steps * (-1));
						//IF THERE WAS OBSTRUCTION, HANDLE IT
						handle_collision(&map, &current_map, &screen_pos, next_tile, &score, &loop);
					}
					else {
						bad_input();
					}
				}
				else {
					bad_input();
				}
			}
			//ELSE
			else {		
				bad_input();
			} 
		}
		else {
			bad_input();
		} 
    }
    
    free(user_input);
    return 0;
}