#Number of Sets
3
#Sets (Question, Response if Answer Correct, Solution)
Hi sweetie! You want to help me cook? How about this... Look 	at this onion I just cut in half. If we count from the center,	it has 6 layers. We put it on the flat side and then we cut it	 horizontally at the exact height where the top of one layer	 meets another. That's 5 cuts. How many individual pieces of	 onion will we get if we ruffle it up into rings and pieces?	                      What do you think?
			        That is right! You are such a smart child!
18
	   Come, lad, sit with grampa! Do you want to help me solve 	   this problem? Yes? Great! Look at this. If a wine barrel 	 soaks up and evaporates one twentieth (1/20) of its contents 	  every two months, how many cups of wine would remain after 	      one year, if it was initially filled with 260 cups?
		      Oh no! My wine! Help me up! We have to do beat the 	      odds. Better drink it quickly, lest it dissappear. 	                   Don't tell your grandma!
182
	  Hey there champ! What? You want to play? Sure. But I need 	  to finish my daily reading first. How many pages? You tell 	  me! If my book has an introduction of 37 pages, and I read 	   43 pages of story every day... Which page number will be 	         the last I read today, if I am on page 375?
			     Hehe, correct as always! You are such a great kid.
381
#END