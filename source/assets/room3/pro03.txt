#Number of Sets
10
#Sets (Question, Response if Answer Correct, Solution)
		These things apparently have a 2 in 5 success rate of hitting 	 the target. I now have got 5 hits out of 20 bolts. How many 	   sets of 1 out of 2 (1/2) succesful hits would I have to 	          shoot to get to the target success rate?
			                  Thanks! Let me focus now!
15
    We were feeling optimistic, so me and my two brothers 	    arranged for a casket just in case. It now turns out I 	     will be needing two. Unless... Could two people with 	    measurements of 15 palms high, 4 wide, and 2 sideways, 	   fit in a casket? What would be the volume needed for two 	       people, if one had an arm of (7x1x1) carried off 	            and the other a leg of (9x2x2) volume?
			      Oh, don't worry, they will fit snugly since they 	          are already dismembered. Poor fellows.
677
		 Throwing rocks is actually very effective. Four out of five 	  times I manage to hit the enemy in the head and stun them, 	 and then the archers take care of them 3 out of 4 times. If 	       I have 40 rocks, how many kills can I assist in?
			                        Not bad, not bad.
24
		    My plan is to feint once and then attack the enemy 24 	    times. After that I feint thrice, and attack 20 times, 	    then I feint five times, and attack 16 times. But when 	    I feint 7 times, then... How many times must I attack?
			                   Exactly! Perfect plan!
12
	  They just keep coming. Already I have to shoot every second 	  enemy at point blank. But at least I can retreive my arrows 	  and reuse them. If every third of my longshots is succesful,	  how many... Ha! I just got one at point blank!... how many 	  enemies do you think I can take out with my ... 11 arrows?
			        Huh. Maybe I should just wait them out and 	             shoot everyone at point blank...
13
		   If a non-fatal wound leaks two cups of blood before being 	    mended, how many non-fatal wounds would be necessary to 	     fall a grown man with 20 cups of blood in his veins, 	             and 12 needed to keep him standing?
			    Less than I thought. Maybe my assumptions were wrong.
4
		   Look at all those swords just lying there. There must be 	   200 at least. If I can carry 21 at a time, and the others 	   take 15 each time I am not there, how many swords do you 	              think I could collect for myself?
			                      A true collection.
125
		  Maybe I should hurl stones down the hill. I saw a rock with 	   dimensions of about (25x16x12) palms in volume. How many 	           equally large stones of (1x2x2) volume 	              do you think I could get from it?
			         Don't worry, it is porous, crumbly rock, but 	           strong enough to bash a head or two in.
1200
	  This thing is set to fly 97 paces, but it has bad accuracy. 	   For each finger-width I make the string longer, the range 	     lowers by 1 pace plus the number of paces equal to the 	       finger-widths added since the beginning. How many 	        widths should I add to the string, so the range 	               is equal or just above 30 paces?
			         The accuracy is much better now! Thank you!
10
		  I miss a lot, but every third time I manage to get a double 	  hit. If a fourth of the hit people recover and keep moving 	  without dieing, then how many did I kill if I shot 30 bolts?
			                Ah, so it evens out eventually... 	                But look, they make another push!
15
#END