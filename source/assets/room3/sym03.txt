#Dimensions (Width Height)
100 39
#Map Content
 X                                      |                                                           
   xx     x X    X X                  X |                                                           
X      X       X X X                    | |X|                                                       
  x  x      x    X X                   X| |||                                                       
        x  X     X X                    | | |                                                       
X   x        X   X X                    | | |                                                       
      X  x x     X X                   X| | |                                                       
            X   X                       |                                                           
 X  x   X       x                       ||                                                          
     x    x   X                        X||||                          X  X                          
        x                                |||||||     |||||||                                        
  x  X  X XX  X X                         ||||||    ||||||||||    |X|                 |X|           
X    x                          x       XX  ||||    |||||||||||   |||                 |||           
   X    X  XX X X    X    X                X   |    ||      ||||  | |                 | |           
   x   x                                  X    |  X |         ||||||||||||||||||||||||||||||||||||||
  X        X    XX     x               XXX                     |||||||||||||||||||||||||||||||||||||
       X                 X            X                         ||||||||||||||||||||||||||||||||||||
 x  x   x       x                                               X X X  X     x        x             
   X      XX      X   X                                         X X      XX              x   XX  x  
        X                                                       X X X           X             x   x 
       XX     x          x                                      X         XX                        
    X                                   X                     X    X                     XX  X      
        X      XX                         X                                     x                X  
    x        x                                                          XX    XX                    
         X       X   x                                 X  X   X                                 X   
         x        X                 X   X   X        X             X                                
                           X                                         x         X            x  X    
   X            X            X                                                      X          X  X 
                                                                  X               x               X 
         x        X       x     X           X       x        X   x                    XX          X 
  X                                  x           X         x           X                    x       
       X           X          XX          X    x    X            X               x    X           X 
 X     x   X  x     X           X             X     X                  x                  X         
   X    X          X  x                      x                      X              X    x           
          x X  X                     x  Xx         X   X x  X               X  x   X   X     X      
      X   x                X     X      X    X                    x     X            X  X       x   
    x      XX        X     x                     x  X    x   XX           x    X x   X    XX   X XX 
 x     X         XX x    X       xx X      x   X                 x    X           x     X    x      
    X         XX     X    XX   XXXXX  X   X   X X  X  Xx  X  X  XX  X     X   X      x          x   
#END