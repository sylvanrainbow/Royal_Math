The Game consists of the .c file, the manifest file, and all the contents (fg, bg, col, sym, rm, pro, dia, obs) for each level.
All need to be present for it to work.
Run the .c file with ./ after enabling execution permissions.
The Game is 25 Lines High so adjust the Terminal to see the whole border (71x25)


MANIFEST FILE
	- contains paths to the other files
	- path to manifest file set in code file

FG & BG
	- foreground and background color files use the .ppm format
	- both raw and ascii types are supported
	- RGB is automatically converted to 8-bit ANSI colors

COLLISION
	- uses the .pgm format (0-255 grayscale)
	- shades of gray are easy to make in RGB by setting all numbers to the same value
	- meanings:
		0 		- free space
		255 	- wall
		49-1 	- obstacle
		50-99 	- room
		100-199 - problem / question
		200-253 - comment / dialogue
		254		- game finish

SYMBOLS / MAP CONTENT
	- text file of ASCII characters which will be displayed in the FG color

ROOM TRANSITION
	- text file with indexes of rooms (starting from 0) to transition to when you collide with a 50-99 block

PROBLEMS / QUESTIONS
	- text file with sets of 3 lines per problem consisting of:
		1. problem
		2. answer
		3. solution
	- text is formatted with tabs (\t) representing line breaks. Add spaces to the left of text to center it in its borders of (63x9)
	- after you give a correct answer, your score gets incremented by 1

DIALOGUE / COMMENTS
	- text file with sets of 3 lines consisting of:
		1. response if player doesn't meet the score requirement
		2. response if he does
		3. score requirement
	- when you meet a score requirement, the connected obstacle gets removed (block 200 corresponds to block 49, 201 to 48...)
	- text is formatted with tabs (\t) representing line breaks. Add spaces to the left of text to center it in its borders of (63x9)

OBSTRUCTION
	- text file with responses if the player hits an obstacle uncleared by dialogue
	- text is formatted with tabs (\t) representing line breaks. Add spaces to the left of text to center it in its borders of (63x9)

WARNINGS
	- level size should be equal or larger than the display size (71x25)
	- in text files, put something like #END at the end because the final line of content has to have a \n as the last character, and not EOF
	- you should never have more dialogue sets than obstacle sets. A dialogue will search for a corresponding obstacle, and if it doesn't exist, that's bad

TIPS
	- adding more obstructions than there are dialogues enables you to have special comments for obstacles or static responses/monologues
	- you can have several areas in the same map, just have the room transition transition to different coordinates of the same room
	- meet the room size requirement by just painting the rest of the room black
	- Aseprite has an option for displaying 1:2 pixels, which makes it easier to see how the finished room will look in a terminal, but it has no .ppm and .pgm exports, so you will have to copy them to Gimp.
	- you can have several of the same tiles for room transfer, or even problem or dialogue. They can be in different places and will behave the same (soldier, terminal, teleport, switches)
	- a problem doesn't have to be a math problem. It can be a riddle you uncover or a passcode.